package br.com.calculadora.calculadora.DTOs;

public class RespostaDTO {

    private Integer resultado;

    public RespostaDTO(Integer resultado) {
        this.resultado = resultado;
    }

    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }

    public RespostaDTO() {
    }
}

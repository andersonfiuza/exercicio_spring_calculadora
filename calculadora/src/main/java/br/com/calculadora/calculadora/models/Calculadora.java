package br.com.calculadora.calculadora.models;

import java.util.List;

public class Calculadora
{
    public Calculadora() {}

    private List<Integer>  numeros;

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }

}


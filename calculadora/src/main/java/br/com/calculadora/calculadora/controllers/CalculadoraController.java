package br.com.calculadora.calculadora.controllers;

import br.com.calculadora.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.calculadora.models.Calculadora;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import br.com.calculadora.calculadora.services.CalculadoraService;


@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() < 1) {
            throw new ResponseStatusException(HttpStatus.MULTI_STATUS.BAD_REQUEST,"È necessário pelo menos dois numeros");
        }

        return calculadoraService.somar(calculadora);

    }

    @PostMapping("/sub")
    public RespostaDTO subtradir(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() < 1) {
            throw new ResponseStatusException(HttpStatus.MULTI_STATUS.BAD_REQUEST,"È necessário pelo menos dois numeros");
        }

        return calculadoraService.subtrair(calculadora);

    }

    @PostMapping("/multiplica")
    public RespostaDTO multiplica(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() < 1) {
            throw new ResponseStatusException(HttpStatus.MULTI_STATUS.BAD_REQUEST,"È necessário pelo menos dois numeros");
        }

        return calculadoraService.multiplicacao(calculadora);

    }

    @PostMapping("/divisao")
    public RespostaDTO divisao(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() < 1) {
            throw new ResponseStatusException(HttpStatus.MULTI_STATUS.BAD_REQUEST,"È necessário pelo menos dois numeros");
        }

        return calculadoraService.divisao(calculadora);

    }

}

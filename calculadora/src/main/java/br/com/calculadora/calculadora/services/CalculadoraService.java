package br.com.calculadora.calculadora.services;

import br.com.calculadora.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora) {

        int resultado = 0;

        for (Integer numero : calculadora.getNumeros()) {
            resultado = resultado + numero;


        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return  respostaDTO;

    }

    public RespostaDTO subtrair(Calculadora calculadora) {

        int resultado = 0;

        for (Integer numero : calculadora.getNumeros()) {
            resultado =  numero -resultado;

            System.out.println(resultado);
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return  respostaDTO;

    }

    public RespostaDTO multiplicacao(Calculadora calculadora) {

       int resultado = 1;
        for (Integer numero : calculadora.getNumeros()) {

            resultado = resultado * numero  ;
//            for(int i =  0; i <  calculadora.getNumeros().size();i++)
//            {
//                resultado = resultado * numero  ;
//            }
//


            System.out.println(resultado);
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return  respostaDTO;

    }

    public RespostaDTO divisao(Calculadora calculadora) {

        int resultado = 1;


        for (Integer numero : calculadora.getNumeros()) {

            if (resultado ==0) resultado=1;
            System.out.println(numero + "/" + resultado);
            resultado = numero / resultado;

            System.out.println(resultado);
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return  respostaDTO;

    }

//    public static void multiplica()
//    {
//        for (int i = 0; i < str.size(); i++)
//        {
//            int resultado=1;
//
//            for(int b = 0; b < str.get(i).length(); b++)
//            {
//                resultado *= Integer.parseInt(str.get(i).substring(b, b + 1));
//            }
//
//            System.out.println(resultado);
//        }
//
//    }
}
